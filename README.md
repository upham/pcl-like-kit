# Pseudo-Cl likelihood #

Python code implementing the exact pseudo-Cl likelihood for correlated Gaussian fields described in [arXiv:1908.00795](https://arxiv.org/abs/1908.00795).

Repository migrated to https://github.com/robinupham/pseudo_cl_likelihood
